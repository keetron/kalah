Very much a basic, self contained Kalah backend application using Spring Boot.

Run the application using `mvn spring-boot:run`. Otherwise, run the application in intellij by starting the KalahApplication.java (rightclick, run).



To start a new game, open `http://localhost:8080/newGame`, it will respond with the active player and board setup.
To make a move, open `http://localhost:8080/playerMove?house=x`, where x is the house you want to sow from. The application will respond with a 404 in case of an illegal move.

If the game is completed, the app will respond with a winner.