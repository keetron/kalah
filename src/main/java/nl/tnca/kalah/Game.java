package nl.tnca.kalah;

import com.google.gson.Gson;

/**
 * This class takes care of a game instance
 */
class Game {

    private int[] board;

    private Player player = Player.PLAYER1;
    private boolean playAgain = false;
    private Rules rules = new Rules();

    Game() {
        this.board = new int[]{6, 6, 6, 6, 6, 6, 0, 6, 6, 6, 6, 6, 6, 0};
    }

    /**
     * player and board status for this game
     *
     * @return a list containing the above
     */
    String getGameState() {
        GameState gameState = new GameState();
        gameState.setPlayer(player);
        gameState.setBoard(board);
        Gson gson = new Gson();
        return gson.toJson(gameState);
    }

    /**
     * the action to sow from a house
     *
     * @param house the house selected by the player (assumed validated)
     */
    void sow(int house) {
        int currentBoard[] = board;
        int beans = currentBoard[house];
        currentBoard[house] = 0;

        while (beans > 0) {
            //first move a house forward from the house we took the beans from
            house += 1;

            //then check if the house is a Kalah for the opposing player
            if (player == Player.PLAYER1 && house == 13 || player == Player.PLAYER2 && house == 6) {
                //skip the Kalah
                house += 1;
            }

            //we might now have gone full circle and need to start at the first house again
            if (house == 14) {
                house = 0;
            }

            //add a bean to the current house...
            currentBoard[house] += 1;
            ///and remove it from the beancounter
            beans -= 1;
        }

        //check if the beancounter ended in an empty house and now has a content of 1
        //if so, take the opposite beans and add to the current players kalah
        currentBoard = rules.endedEmptySpot(house, board, player);


        //check if the beancounter ended in a Kalah
        //if so, set play again to true, else to false
        if (house == 6 || house == 13) {
            playAgain = true;
        }

        //now all is done, update the gameboard
        board = currentBoard;

    }

    /**
     * who is the next player, based on playAgain and current player
     */
    void setNextPlayer() {
        //leave the player if he can play again
        if (playAgain) {
            //first toggle playAgain to false
            playAgain = false;
            return;
        }

        //here switch to to the other if playAgain is false
        if (player == Player.PLAYER1) {
            player = Player.PLAYER2;
        } else {
            player = Player.PLAYER1;
        }
    }

    boolean movePossible(int house) {
        return rules.movePossible(board, house, player);
    }

    boolean gameEnd() {
        return rules.gameEnd(board);
    }

    Player winner() {
        return rules.whoWins(board);
    }

    /**
     * Setters and getters for testing purposes
     *
     * @param player the player to set the variable player to
     */
    void setPlayer(Player player) {
        this.player = player;
    }

    Player getPlayer() {
        return player;
    }

    void setPlayAgain(boolean playAgain) {
        this.playAgain = playAgain;
    }

    boolean getPlayAgain() {
        return playAgain;
    }

    int[] getBoard() {
        return board;
    }

    void setBoard(int[] board) {
        this.board = board;
    }
}
