package nl.tnca.kalah;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class GameController {
    private Game game = new Game();

    /**
     * endpoint to start a new game
     *
     * @return the game state, first player number and then the board setup
     */
    @RequestMapping("/newGame")
    public String newGame() {
        game = new Game();
        return game.getGameState();
    }

    /**
     * endpoint to receive the move of the player
     *
     * @param house the house the player wants to pick from
     * @return the game state, first player number and then the board setup
     */
    @RequestMapping("/playerMove")
    public Object playerMove(@RequestParam(value = "house") int house) throws Exception {
        //first check if the input is valid for this player
        if (!game.movePossible(house)) {
            throw new Exception("This is an invalid move. Please try again with a valid move. " + game.getGameState());
        }

        //now take the beans and run!
        game.sow(house);

        //did the game end and if yes, who won?
        if (game.gameEnd()) {
            return "The winner is: " + game.winner();
        }

        game.setNextPlayer();
        return game.getGameState();
    }
}
