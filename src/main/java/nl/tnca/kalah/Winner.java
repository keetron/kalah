package nl.tnca.kalah;

public class Winner {
    private Player winner;

    Winner(Player player) {
        this.winner = player;
    }

    public Player getWinner() {
        return winner;
    }
}
