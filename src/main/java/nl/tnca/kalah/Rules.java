package nl.tnca.kalah;

import java.util.Arrays;
import java.util.List;

/**
 * This class contains rules that could be put in a lower level
 * also to enable easier testing for future refactoring
 */
class Rules {

    private static final List<Integer> PLAYER2HOUSES = Arrays.asList(7, 8, 9, 10, 11, 12);
    private static final List<Integer> PLAYER1HOUSES = Arrays.asList(0, 1, 2, 3, 4, 5);

    /**
     * If the game ended, who is the winner
     *
     * @return 0 for a draw, else the winning player number
     */
    Player whoWins(int[] board) {
        //get the board and total the houses' and Kalah content per player

        int player1HouseContent = board[0] +
                board[1] +
                board[2] +
                board[3] +
                board[4] +
                board[5] +
                board[6];

        int player2HouseContent = board[7] +
                board[8] +
                board[9] +
                board[10] +
                board[11] +
                board[12] +
                board[13];

        if (player1HouseContent == player2HouseContent) {
            return new Winner(Player.NONE).getWinner();
        }
        if (player1HouseContent > player2HouseContent) {
            return new Winner(Player.PLAYER1).getWinner();
        }
        return new Winner(Player.PLAYER2).getWinner();
    }

    int[] endedEmptySpot(int house, int[] board, Player player) {
        if (board[house] == 1) {
            //is this house owned by the current player?
            if (player == Player.PLAYER1 && (house == 0 || house == 1 || house == 2 || house == 3 || house == 4 || house == 5)) {
                board[6] = board[6] + board[12 - house];
                board[12 - house] = 0;
            }

            if (player == Player.PLAYER2 && (house == 7 || house == 8 || house == 9 || house == 10 || house == 11 || house == 12)) {
                board[13] = board[13] + board[12 - house];
                board[12 - house] = 0;
            }
        }
        return board;
    }


    /**
     * method to determine if this house is a valid house to move from
     * Player 1 has house 1 to 6 (in board slot 0 to 5), player 2 house 7 to 12 (in board slot 7 to 12)
     *
     * @param board  the current board
     * @param house  the selected house
     * @param player the current player
     * @return if the move is possible
     */
    boolean movePossible(int[] board, int house, Player player) {

        //check if the house doesn't happen to be empty
        if (board[house] == 0) {
            return false;
        }

        //check if the house is owned by player 1
        if (player == Player.PLAYER1) {
            return PLAYER1HOUSES.contains(house);
        }

        //check if the house is owned by player 2
        if (player == Player.PLAYER2) {
            return PLAYER2HOUSES.contains(house);
        }

        //if it is neither of the above, it must be a Kalah
        return false;
    }

    /**
     * Simply to check if the game ended and a winner can be determined
     *
     * @return true if the game ended
     */
    boolean gameEnd(int[] board) {
        //get the board and total the houses' content per player

        int player1HouseContent = board[0] +
                board[1] +
                board[2] +
                board[3] +
                board[4] +
                board[5];

        int player2HouseContent = board[7] +
                board[8] +
                board[9] +
                board[10] +
                board[11] +
                board[12];

        return player1HouseContent == 0 || player2HouseContent == 0;
    }


}
