package nl.tnca.kalah;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class GameTest {
    private int[] board;
    private Game game;

    @Before
    public void setUp() {
        board = new int[]{6, 6, 6, 6, 6, 6, 0, 6, 6, 6, 6, 6, 6, 0};
        game = new Game();
    }

    @Test
    public void getGameState() {
    }

    @Test
    public void sowBasic() {
        game.setPlayer(Player.PLAYER1);
        game.setBoard(board);
        game.sow(1);
        Assert.assertArrayEquals(new int[]{6, 0, 7, 7, 7, 7, 1, 7, 6, 6, 6, 6, 6, 0}, game.getBoard());
        Assert.assertFalse(game.getPlayAgain());
    }

    @Test
    public void sowPlayAgain() {
        game.setPlayer(Player.PLAYER1);
        int[] board = {6, 6, 6, 6, 2, 6, 0, 6, 6, 6, 6, 6, 6, 0};
        game.setBoard(board);
        game.sow(4);
        Assert.assertArrayEquals(new int[]{6, 6, 6, 6, 0, 7, 1, 6, 6, 6, 6, 6, 6, 0}, game.getBoard());
        Assert.assertTrue(game.getPlayAgain());
    }

    @Test
    public void sowAcrossOtherKalahAndPlayAgain() {
        game.setPlayer(Player.PLAYER2);
        int[] board = {6, 6, 6, 6, 6, 6, 0, 6, 6, 6, 6, 6, 14, 0};
        game.setBoard(board);
        game.sow(12);
        Assert.assertArrayEquals(new int[]{7, 7, 7, 7, 7, 7, 0, 7, 7, 7, 7, 7, 1, 2}, game.getBoard());
        Assert.assertTrue(game.getPlayAgain());
    }

    @Test
    public void sowAcrossOtherKalah() {
        game.setPlayer(Player.PLAYER1);
        int[] board = {6, 6, 6, 6, 6, 8, 0, 6, 6, 6, 6, 6, 6, 0};
        game.setBoard(board);
        game.sow(5);
        Assert.assertArrayEquals(new int[]{7, 6, 6, 6, 6, 0, 1, 7, 7, 7, 7, 7, 7, 0}, game.getBoard());
        Assert.assertFalse(game.getPlayAgain());
    }


    @Test
    public void setNextPlayer() {
        game.setPlayAgain(false);
        game.setPlayer(Player.PLAYER2);
        game.setNextPlayer();
        Assert.assertEquals(Player.PLAYER1, game.getPlayer());
        Assert.assertFalse(game.getPlayAgain());
    }


    @Test
    public void setNextPlayer2() {
        game.setPlayAgain(false);
        game.setPlayer(Player.PLAYER1);
        game.setNextPlayer();
        Assert.assertEquals(Player.PLAYER2, game.getPlayer());
        Assert.assertFalse(game.getPlayAgain());
    }

    @Test
    public void repeatPlayer2() {
        game.setPlayAgain(true);
        game.setPlayer(Player.PLAYER2);
        game.setNextPlayer();
        Assert.assertEquals(Player.PLAYER2, game.getPlayer());
        Assert.assertFalse(game.getPlayAgain());
    }

    @Test
    public void repeatPlayer1() {
        game.setPlayAgain(true);
        game.setPlayer(Player.PLAYER1);
        game.setNextPlayer();
        Assert.assertEquals(Player.PLAYER1, game.getPlayer());
        Assert.assertFalse(game.getPlayAgain());
    }

}