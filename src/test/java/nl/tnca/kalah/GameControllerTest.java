package nl.tnca.kalah;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;


public class GameControllerTest {

    private Game game;
    private GameController gameController;

    @Before
    public void setUp() {
        game = new Game();
        gameController = new GameController();
    }


    @Test
    public void newGame() {
        String gameState = gameController.newGame();
        Assert.assertEquals("Response should contain a new board", "{\"player\":\"PLAYER1\",\"board\":[6,6,6,6,6,6,0,6,6,6,6,6,6,0]}", gameState);
    }

    @Test
    public void newGameDuringRunningGame() {
        //after starting the game, make a move
        game.sow(2);

        //start a new game
        String gameState = gameController.newGame();
        Assert.assertEquals("Response should contain a new board", "{\"player\":\"PLAYER1\",\"board\":[6,6,6,6,6,6,0,6,6,6,6,6,6,0]}", gameState);
    }

    @Test
    public void playerMove() {
    }
}