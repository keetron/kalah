package nl.tnca.kalah;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class RulesTest {
    private int[] board;
    private Rules rules = new Rules();

    @Before
    public void setUp() {
        board = new int[]{6, 6, 6, 6, 6, 6, 0, 6, 6, 6, 6, 6, 6, 0};
    }

    @Test
    public void movePossible() {
        Assert.assertTrue(rules.movePossible(board, 1, Player.PLAYER1));
        Assert.assertTrue(rules.movePossible(board, 11, Player.PLAYER2));
    }

    @Test
    public void drawTest() {
        Player winner = rules.whoWins(board);
        Assert.assertEquals("This hsould be a draw", winner, Player.NONE);
    }

    @Test
    public void playerWinnerTest() {
        int[] board = {6, 0, 6, 6, 6, 6, 0, 6, 6, 6, 6, 6, 6, 0};
        Player winner = rules.whoWins(board);
        Assert.assertEquals("Player 2 should win", winner, Player.PLAYER2);
    }

    @Test
    public void moveNotPossible() {
        Assert.assertFalse(rules.movePossible(board, 11, Player.PLAYER1));
        Assert.assertFalse(rules.movePossible(board, 1, Player.PLAYER2));
        Assert.assertFalse(rules.movePossible(board, 6, Player.PLAYER1));
        Assert.assertFalse(rules.movePossible(board, 6, Player.PLAYER2));
        Assert.assertFalse(rules.movePossible(board, 13, Player.PLAYER1));
        Assert.assertFalse(rules.movePossible(board, 13, Player.PLAYER2));
    }


    @Test
    public void gameEnds() {
        //player1 no longer has beans
        int[] board = {0, 0, 0, 0, 0, 0, 0, 6, 6, 6, 6, 6, 6, 0};
        Assert.assertTrue(rules.gameEnd(board));

        //player2 no longer has beans
        board = new int[]{6, 6, 6, 6, 6, 6, 0, 0, 0, 0, 0, 0, 0, 0};
        Assert.assertTrue(rules.gameEnd(board));
    }

    @Test
    public void gameDoesNotEnd() {
        int[] board = {6, 0, 6, 6, 6, 6, 0, 6, 6, 6, 6, 6, 6, 0};
        Assert.assertFalse(rules.gameEnd(board));
    }

    @Test
    public void player1EndedEmptySpot() {
        int[] board = {6, 1, 6, 6, 6, 6, 0, 6, 6, 6, 6, 6, 6, 0};
        board = rules.endedEmptySpot(1, board, Player.PLAYER1);
        int[] expectedResult = {6, 1, 6, 6, 6, 6, 6, 6, 6, 6, 6, 0, 6, 0};
        Assert.assertArrayEquals(expectedResult, board);
    }

    @Test
    public void player2EndedEmptySpot() {
        int[] board = {6, 1, 6, 6, 6, 6, 0, 1, 6, 6, 6, 6, 6, 0};
        board = rules.endedEmptySpot(7, board, Player.PLAYER2);
        int[] expectedResult = {6, 1, 6, 6, 6, 0, 0, 1, 6, 6, 6, 6, 6, 6};
        Assert.assertArrayEquals(expectedResult, board);
    }

    @Test
    public void player1DidNotEndedEmptySpot() {
        int[] board = {6, 1, 6, 6, 6, 6, 0, 1, 6, 6, 6, 6, 6, 0};
        int[] resultBoard = rules.endedEmptySpot(5, board, Player.PLAYER1);
        //There should be no change to the board, so we can use the setup board as expected result
        Assert.assertArrayEquals(resultBoard, board);
    }

    @Test
    public void player2DidNotEndedEmptySpot() {
        int[] board = {6, 1, 6, 6, 6, 6, 0, 1, 6, 6, 6, 6, 6, 0};
        int[] resultBoard = rules.endedEmptySpot(8, board, Player.PLAYER2);
        //There should be no change to the board, so we can use the setup board as expected result
        Assert.assertArrayEquals(board, resultBoard);
    }

}